﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBProject
{
    //i metodi non saranno void ma restituiranno sempre una tabella
    interface Model
    {
        /**
         * Permette di vedere le partite in cui un giocatore è convocato a partire dalla data corrente
         */
        void ShowPlayerMatch(DataGridView grid);
        /**
         * Permette di vedere le partite in cui è impegnata la categoria allenata dall'allenatore a partire dalla data corrente
         */
        void ShowCoachMatch(DataGridView grid);

        /**
         * permette di inserire un allenamento
         */
        void addTrainig(string spazio, string data, string orario, string durata, string descrizione); 

        /**
         * Permette di vedere gli eventi straordinari che si svolgeranno al campo a partire dalla data corrente
         */
        void ShowSpecialEvent(DataGridView grid);
        /**
         * permette di vedere le informazioni relative ai vari campionati
         */
        void ShowChampionships(DataGridView grid);
        /**
         * vedere chi a oggi deve fare la visita medica(non la fa da più di un anno)
         */
        void ShowWhoNeedsMedicalPermission(DataGridView grid);
        /*
         * vedere chi a oggi non ha pagato la quota di iscrizione(non la paga da più di un anno)
         */
        void ShowWhoHaveToPay(DataGridView grid);
        /**
         * Inserire allenatore
         */
        void NewCoach(String tesseraFir, String nome, String cognome, String CF);
        /**
         * InserireNuovoGiocatore
         */
        void NewPlayer(String tesseraFir, String nome, String cognome, String CF, string categoria, string ruolo, string lastPayment, String lastMedicalVisit);
        /**
         * Creare una categoria
         */
        void AddCategory(int maxAge, int minAge, int cost);
        /**
         * assegnare allenatore a una categoria
         */
        void AssingCoach(String tesseraFir, string Categoria);
        /**
         * Aggiungere una partita
         */
        void AddMatch(string categoria, string stagione, string orario, string data, char inCasa, string avversaria);
        /**
         * aggiungere un evento partita (manca il codice partita)
         */
        void AddMatchEvents(String player, String type, string second, string season, string date, string data_fine);
        /**
         * convocare un giocatore 
         */
        void CallAPlayer(String player, string partita);
        /**
         * iscrivere una categoria a un nuovo campionato
         */
        void AddNewChampionship(string annoInizio, string Categoria);
        /**
         * vedere le asseganzioni categoria giocatori
         */
        void ShowCategoryCoach(DataGridView grid);

        /**
         * controllare che esista quel giocatore e reperire i suoi dati
         */
        void checkPlayerLogin(String tesseraFir);

        /**
        * controllare che esista quell'allenatore e reperire i suoi dati
        */
        void checkCoachLogin(String tesseraFir);

        /**
        * controlla che la password inserita sia quella corretta
        */
        void checkLeaderLogin(String password);

        /**
         *mostra gli allenamenti per l'allenatore o il giocatore corrente
         */
        void ShowTraining(DataGridView grid);
        /**
         *resetta la sessione
         */
        void reset();
        /**
         * mostra i giocatori di una certa categoria
         */
        void showCategoryPlayers(DataGridView grid);
        /**
         * mostra le statistiche di un giocatore dato in ingresso il suo codice
         */
        void PlayerStatistics(string tesseraFir, DataGridView grid);

        /**
         * mostra le statitiche del giocatore corrente
         */
        void CurrentPlayerStatistics(DataGridView grid);
        /**
         * aggiunge un evento eccezionale
         */
        void AddSpecialEvents(string data, string responsabile, string nomeEvento, string time, string duration, string spazio);
        /**
         * visualizzare le prenotazioni effettuate a partire dalla data corrente
         */
        void showPrenotazioni(DataGridView grid);





    }
}
