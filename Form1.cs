﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject
{
    public partial class Form1 : Form
    {
        private List<Panel> panels = new List<Panel>();
        private Panel current;
        private Panel prev;
        private List<DataGridView> grids = new List<DataGridView>();
        private List<TextBox> tb = new List<TextBox>();
        private Model m = new ModelImpl();
        public Form1()
        {
            
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            tb.AddRange((new TextBox[] { LoadingTextBox, DateTrainingText, TimeTrainingText,
                DurationTrainingText, PaceTrainingText, textBoxMatchCalling, textBoxPlayerCalling, PlayerCardText,
                CoachFirCardAssingText, CategoryAssigText, seasonTextMatch, categoryTextMatch, dateTextMatch,
                tempoTextMatch, avversariaTextMatch, FirCardPlayerText, PlayerNameText, PlayerSurnameText,
                PlayerCFText, PlayerRoleText, LastMedicalCheckPlayerText, LastPaymentPlayerText, PlayerCategoryText,
                FirCardCoachText, CoachNameText, CoachSurnameText, CoachCFText, MaxAgeTextBox, MinAgeTextBox,
                PaymentTextBox, StartingDateText, CategoryText, data_fine, DataEventTX, ResponsabileTX, NomeEventoTX, TempoEventsTX,
                DurataEventTX, SpazioEventsTX}));
            tb.Add(textBoxPlayerCalling);
            tb.Add(textBoxMatchCalling);
            tb.Add(PlayerCardText);
            tb.Add(PlayerFirCardEventText);
            tb.Add(EventText);
            tb.Add(SecondEventText);
            tb.Add(SeasonEventsText);
            tb.Add(DateEventsText);
            grids.Add(dataGridViewLeaderMore);
            grids.Add(StatisticGridCoach);
            grids.Add(dataGridViewCoach);
            grids.Add(dataGridViewPlayer);
            grids.Add(gridMatchesCallingView);
            grids.Add(gridPlayerCallingView);
            panels.Add(PanelLogin);
            panels.Add(PanelPlayer);
            panels.Add(CoachPanel);
            panels.Add(LeaderPanel);
            panels.Add(CallingPanel);
            panels.Add(MoreLeaderPanel);
            SwitchPanel(PanelLogin);
            fillTheGrid();
            m.showPrenotazioni(gridPrenotazioniLeader);
            m.showPrenotazioni(gridPrenotazioniCoach);
            this.Size = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            this.Location = new Point(0, 0);
        }

        private void SwitchPanel(Panel p)
        {
            panels.ForEach(x => x.Visible = false);
            p.Visible = true;
            this.prev = this.current;
            this.current = p;
            grids.ForEach(g => g.DataSource = null);
        }
        private void GenerateMessageBoxForException(Exception e)
        {
            MessageBox.Show(e.Message);
        }

        private void fillTheGrid()
        {
            grids.ForEach(g => g.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill);
        }
        private void resetTextBox()
        {
            tb.ForEach(t => t.Text = "");
        }
        //utilità
        private void PlayerButton_Click(object sender, EventArgs e)
        {
            SwitchPanel(PanelPlayer);
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            SwitchPanel(PanelLogin);
            m.reset();
            resetTextBox();
        }

        private void PrevButton_Click(object sender, EventArgs e)
        {
            if (!current.Equals(PanelLogin))
            {
                if (prev == PanelLogin) { m.reset(); resetTextBox(); }
                SwitchPanel(prev);
            } 
            
        }

    
        //player view
        private void MatchesPlayerButton_Click(object sender, EventArgs e)
        {
            m.ShowPlayerMatch(dataGridViewPlayer);
        }

        private void TrainigPlayerButton_Click(object sender, EventArgs e)
        {
            m.ShowTraining(dataGridViewPlayer);
        }

        private void EventsPlayerButton_Click(object sender, EventArgs e)
        {
            m.ShowSpecialEvent(dataGridViewPlayer);
        }
        private void StatisticButtonPlayer_Click(object sender, EventArgs e)
        {
            m.CurrentPlayerStatistics(dataGridViewPlayer);
        }
        //coach view
        private void EventsCoachButton_Click(object sender, EventArgs e)
        {
            m.ShowSpecialEvent(dataGridViewCoach);
        }

        private void TrainigCoachButton_Click(object sender, EventArgs e)
        {
            m.ShowTraining(dataGridViewCoach);
        }

        private void MatchesCoachButton_Click(object sender, EventArgs e)
        {
            m.ShowCoachMatch(dataGridViewCoach);
        }

        private void AddTrainingButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.addTrainig(PaceTrainingText.Text, DateTrainingText.Text, TimeTrainingText.Text,
                                DurationTrainingText.Text, DescriptionTrainingText.Text);
            }
            catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
        }
        private void CollingButton_Click(object sender, EventArgs e)
        {
            SwitchPanel(CallingPanel);
            m.ShowCoachMatch(gridMatchesCallingView);
            m.showCategoryPlayers(gridPlayerCallingView);
        }
        //calling view
        private void CallingButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.CallAPlayer(textBoxPlayerCalling.Text, textBoxMatchCalling.Text);
            }
            catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
        }
        private void PlayerStatisticCoach_Click(object sender, EventArgs e)
        {
            try
            {
                m.PlayerStatistics(PlayerCardText.Text, StatisticGridCoach);
            } catch(Exception ec)
            {
                GenerateMessageBoxForException(ec);
            } 
        }

        private void AddEpisodeButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.AddMatchEvents(PlayerFirCardEventText.Text, EventText.Text, SecondEventText.Text, SeasonEventsText.Text, DateEventsText.Text,
                data_fine.Text);
            }
            catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }

        }
        //login view
        private void PlayerButton_Click_1(object sender, EventArgs e)
        {
            try
            {
                m.checkPlayerLogin(LoadingTextBox.Text);
                SwitchPanel(PanelPlayer);
            } catch (Exception ec)
            {
                MessageBox.Show("Login string for coaches and players is their FIR car code, for leaders is a secret password");
            }
                
        }

        private void CoachButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.checkCoachLogin(LoadingTextBox.Text);
                SwitchPanel(CoachPanel);
            }
            catch (Exception ec)
            {
                MessageBox.Show("Login string for coaches and players is their FIR car code, for leaders is a secret password");
            }
            
        }

        private void LeaderButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.checkLeaderLogin(LoadingTextBox.Text);
                SwitchPanel(LeaderPanel);
            }
            catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }

        }

        //leader View
        private void AddPlayerButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.NewPlayer(FirCardPlayerText.Text, PlayerNameText.Text, PlayerSurnameText.Text, PlayerCFText.Text,
                    PlayerCategoryText.Text, PlayerRoleText.Text, 
                    LastMedicalCheckPlayerText.Text, LastPaymentPlayerText.Text);
            } catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
        }

        private void AddCoachButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.NewCoach(FirCardCoachText.Text, CoachNameText.Text, CoachSurnameText.Text, CoachCFText.Text);
            } catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
        }

        private void AddCategoryButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.AddCategory(Int32.Parse(MaxAgeTextBox.Text), Int32.Parse(MinAgeTextBox.Text), Int32.Parse(PaymentTextBox.Text));
            } catch(Exception ec)
            {
                GenerateMessageBoxForException(ec);
            } 
            
        }

        private void AddChampionShipButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.AddNewChampionship(StartingDateText.Text, CategoryText.Text);
            } catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
            
        }

        private void moreButton_Click(object sender, EventArgs e)
        {
            SwitchPanel(MoreLeaderPanel);
        }
        //more leader view
        private void WhoHaventPayButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.ShowWhoHaveToPay(dataGridViewLeaderMore);
               
            }
            catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
        }

        private void WhoNeedMedicalCheckButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.ShowWhoNeedsMedicalPermission(dataGridViewLeaderMore);
            }
            catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
        }

        private void AssignButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.AssingCoach(CoachFirCardAssingText.Text, CategoryAssigText.Text);
            }
            catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
        }

        private void ShowChampionShipButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.ShowChampionships(dataGridViewLeaderMore);
            }
            catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
        }

        private void CategoryAndCoachButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.ShowCategoryCoach(dataGridViewLeaderMore);

            }
            catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
        }

        private void InsertMatchButton_Click(object sender, EventArgs e)
        {
            char h = homeBoxMatch.Checked ? 's' : 'n';
            List<TextBox> tb = new List<TextBox>();
            tb.Add(categoryTextMatch);
            tb.Add(seasonTextMatch);
            tb.Add(tempoTextMatch);
            tb.Add(dateTextMatch);
            tb.Add(avversariaTextMatch);
            try
            {
                m.AddMatch(categoryTextMatch.Text, seasonTextMatch.Text, tempoTextMatch.Text, dateTextMatch.Text, h, avversariaTextMatch.Text);
                
            } catch(Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
            tb.ForEach(t => t.Text = "");
        }

       private void AddEventButton_Click(object sender, EventArgs e)
        {
            try
            {
                m.AddSpecialEvents(DataEventTX.Text, ResponsabileTX.Text, NomeEventoTX.Text,
               TempoEventsTX.Text, DurataEventTX.Text, SpazioEventsTX.Text);
            } catch (Exception ec)
            {
                GenerateMessageBoxForException(ec);
            }
           
        }
    }
}
