﻿namespace DBProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelLogin = new System.Windows.Forms.Panel();
            this.LoadingTextBox = new System.Windows.Forms.TextBox();
            this.LeaderButton = new System.Windows.Forms.Button();
            this.CoachButton = new System.Windows.Forms.Button();
            this.PlayerButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.PanelPlayer = new System.Windows.Forms.Panel();
            this.StatisticButtonPlayer = new System.Windows.Forms.Button();
            this.MatchesPlayerButton = new System.Windows.Forms.Button();
            this.TrainigPlayerButton = new System.Windows.Forms.Button();
            this.EventsPlayerButton = new System.Windows.Forms.Button();
            this.dataGridViewPlayer = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.CoachPanel = new System.Windows.Forms.Panel();
            this.label55 = new System.Windows.Forms.Label();
            this.gridPrenotazioniCoach = new System.Windows.Forms.DataGridView();
            this.CollingButton = new System.Windows.Forms.Button();
            this.AddTrainingButton = new System.Windows.Forms.Button();
            this.DescriptionTrainingText = new System.Windows.Forms.RichTextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.PaceTrainingText = new System.Windows.Forms.TextBox();
            this.DurationTrainingText = new System.Windows.Forms.TextBox();
            this.TimeTrainingText = new System.Windows.Forms.TextBox();
            this.DateTrainingText = new System.Windows.Forms.TextBox();
            this.MatchesCoachButton = new System.Windows.Forms.Button();
            this.TrainigCoachButton = new System.Windows.Forms.Button();
            this.EventsCoachButton = new System.Windows.Forms.Button();
            this.dataGridViewCoach = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.PrevButton = new System.Windows.Forms.Button();
            this.LoginButton = new System.Windows.Forms.Button();
            this.LeaderPanel = new System.Windows.Forms.Panel();
            this.PlayerCategoryText = new System.Windows.Forms.TextBox();
            this.AddChampionShipButton = new System.Windows.Forms.Button();
            this.CategoryText = new System.Windows.Forms.TextBox();
            this.StartingDateText = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.AddCategoryButton = new System.Windows.Forms.Button();
            this.AddPlayerButton = new System.Windows.Forms.Button();
            this.AddCoachButton = new System.Windows.Forms.Button();
            this.moreButton = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.LastPaymentPlayerText = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LastMedicalCheckPlayerText = new System.Windows.Forms.TextBox();
            this.PlayerRoleText = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CoachCFText = new System.Windows.Forms.TextBox();
            this.PlayerCFText = new System.Windows.Forms.TextBox();
            this.PaymentTextBox = new System.Windows.Forms.TextBox();
            this.CoachSurnameText = new System.Windows.Forms.TextBox();
            this.MaxAgeTextBox = new System.Windows.Forms.TextBox();
            this.PlayerSurnameText = new System.Windows.Forms.TextBox();
            this.MinAgeTextBox = new System.Windows.Forms.TextBox();
            this.FirCardCoachText = new System.Windows.Forms.TextBox();
            this.CoachNameText = new System.Windows.Forms.TextBox();
            this.PlayerNameText = new System.Windows.Forms.TextBox();
            this.FirCardPlayerText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.MoreLeaderPanel = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.gridPrenotazioniLeader = new System.Windows.Forms.DataGridView();
            this.AddEventButton = new System.Windows.Forms.Button();
            this.ResponsabileTX = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.NomeEventoTX = new System.Windows.Forms.TextBox();
            this.DurataEventTX = new System.Windows.Forms.TextBox();
            this.SpazioEventsTX = new System.Windows.Forms.TextBox();
            this.DataEventTX = new System.Windows.Forms.TextBox();
            this.TempoEventsTX = new System.Windows.Forms.TextBox();
            this.InsertMatchButton = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.homeBoxMatch = new System.Windows.Forms.CheckBox();
            this.avversariaTextMatch = new System.Windows.Forms.TextBox();
            this.tempoTextMatch = new System.Windows.Forms.TextBox();
            this.dateTextMatch = new System.Windows.Forms.TextBox();
            this.categoryTextMatch = new System.Windows.Forms.TextBox();
            this.seasonTextMatch = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.ShowChampionShipButton = new System.Windows.Forms.Button();
            this.CategoryAndCoachButton = new System.Windows.Forms.Button();
            this.AssignButton = new System.Windows.Forms.Button();
            this.CategoryAssigText = new System.Windows.Forms.TextBox();
            this.CoachFirCardAssingText = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.WhoNeedMedicalCheckButton = new System.Windows.Forms.Button();
            this.WhoHaventPayButton = new System.Windows.Forms.Button();
            this.dataGridViewLeaderMore = new System.Windows.Forms.DataGridView();
            this.CallingPanel = new System.Windows.Forms.Panel();
            this.label48 = new System.Windows.Forms.Label();
            this.data_fine = new System.Windows.Forms.TextBox();
            this.AddEpisodeButton = new System.Windows.Forms.Button();
            this.EventText = new System.Windows.Forms.TextBox();
            this.SecondEventText = new System.Windows.Forms.TextBox();
            this.PlayerFirCardEventText = new System.Windows.Forms.TextBox();
            this.DateEventsText = new System.Windows.Forms.TextBox();
            this.SeasonEventsText = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.StatisticGridCoach = new System.Windows.Forms.DataGridView();
            this.PlayerStatisticCoach = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.PlayerCardText = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBoxPlayerCalling = new System.Windows.Forms.TextBox();
            this.textBoxMatchCalling = new System.Windows.Forms.TextBox();
            this.CallingButton = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.Matches = new System.Windows.Forms.Label();
            this.gridPlayerCallingView = new System.Windows.Forms.DataGridView();
            this.gridMatchesCallingView = new System.Windows.Forms.DataGridView();
            this.label32 = new System.Windows.Forms.Label();
            this.PanelLogin.SuspendLayout();
            this.PanelPlayer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlayer)).BeginInit();
            this.CoachPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPrenotazioniCoach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCoach)).BeginInit();
            this.LeaderPanel.SuspendLayout();
            this.MoreLeaderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPrenotazioniLeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLeaderMore)).BeginInit();
            this.CallingPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatisticGridCoach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPlayerCallingView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMatchesCallingView)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelLogin
            // 
            this.PanelLogin.Controls.Add(this.LoadingTextBox);
            this.PanelLogin.Controls.Add(this.LeaderButton);
            this.PanelLogin.Controls.Add(this.CoachButton);
            this.PanelLogin.Controls.Add(this.PlayerButton);
            this.PanelLogin.Controls.Add(this.label1);
            this.PanelLogin.Location = new System.Drawing.Point(87, 212);
            this.PanelLogin.Name = "PanelLogin";
            this.PanelLogin.Size = new System.Drawing.Size(706, 178);
            this.PanelLogin.TabIndex = 0;
            // 
            // LoadingTextBox
            // 
            this.LoadingTextBox.Location = new System.Drawing.Point(111, 16);
            this.LoadingTextBox.Name = "LoadingTextBox";
            this.LoadingTextBox.Size = new System.Drawing.Size(179, 22);
            this.LoadingTextBox.TabIndex = 2;
            // 
            // LeaderButton
            // 
            this.LeaderButton.Location = new System.Drawing.Point(324, 98);
            this.LeaderButton.Name = "LeaderButton";
            this.LeaderButton.Size = new System.Drawing.Size(75, 32);
            this.LeaderButton.TabIndex = 1;
            this.LeaderButton.Text = "Leader";
            this.LeaderButton.UseVisualStyleBackColor = true;
            this.LeaderButton.Click += new System.EventHandler(this.LeaderButton_Click);
            // 
            // CoachButton
            // 
            this.CoachButton.Location = new System.Drawing.Point(324, 60);
            this.CoachButton.Name = "CoachButton";
            this.CoachButton.Size = new System.Drawing.Size(75, 32);
            this.CoachButton.TabIndex = 1;
            this.CoachButton.Text = "Coach";
            this.CoachButton.UseVisualStyleBackColor = true;
            this.CoachButton.Click += new System.EventHandler(this.CoachButton_Click);
            // 
            // PlayerButton
            // 
            this.PlayerButton.Location = new System.Drawing.Point(324, 22);
            this.PlayerButton.Name = "PlayerButton";
            this.PlayerButton.Size = new System.Drawing.Size(75, 32);
            this.PlayerButton.TabIndex = 1;
            this.PlayerButton.Text = "Player";
            this.PlayerButton.UseVisualStyleBackColor = true;
            this.PlayerButton.Click += new System.EventHandler(this.PlayerButton_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Login";
            // 
            // PanelPlayer
            // 
            this.PanelPlayer.Controls.Add(this.StatisticButtonPlayer);
            this.PanelPlayer.Controls.Add(this.MatchesPlayerButton);
            this.PanelPlayer.Controls.Add(this.TrainigPlayerButton);
            this.PanelPlayer.Controls.Add(this.EventsPlayerButton);
            this.PanelPlayer.Controls.Add(this.dataGridViewPlayer);
            this.PanelPlayer.Controls.Add(this.label2);
            this.PanelPlayer.Location = new System.Drawing.Point(31, 50);
            this.PanelPlayer.Name = "PanelPlayer";
            this.PanelPlayer.Size = new System.Drawing.Size(1324, 402);
            this.PanelPlayer.TabIndex = 1;
            this.PanelPlayer.Visible = false;
            // 
            // StatisticButtonPlayer
            // 
            this.StatisticButtonPlayer.Location = new System.Drawing.Point(503, 213);
            this.StatisticButtonPlayer.Name = "StatisticButtonPlayer";
            this.StatisticButtonPlayer.Size = new System.Drawing.Size(102, 30);
            this.StatisticButtonPlayer.TabIndex = 5;
            this.StatisticButtonPlayer.Text = "Statistics";
            this.StatisticButtonPlayer.UseVisualStyleBackColor = true;
            this.StatisticButtonPlayer.Click += new System.EventHandler(this.StatisticButtonPlayer_Click);
            // 
            // MatchesPlayerButton
            // 
            this.MatchesPlayerButton.Location = new System.Drawing.Point(404, 213);
            this.MatchesPlayerButton.Name = "MatchesPlayerButton";
            this.MatchesPlayerButton.Size = new System.Drawing.Size(75, 30);
            this.MatchesPlayerButton.TabIndex = 4;
            this.MatchesPlayerButton.Text = "Matches";
            this.MatchesPlayerButton.UseVisualStyleBackColor = true;
            this.MatchesPlayerButton.Click += new System.EventHandler(this.MatchesPlayerButton_Click);
            // 
            // TrainigPlayerButton
            // 
            this.TrainigPlayerButton.Location = new System.Drawing.Point(312, 213);
            this.TrainigPlayerButton.Name = "TrainigPlayerButton";
            this.TrainigPlayerButton.Size = new System.Drawing.Size(75, 30);
            this.TrainigPlayerButton.TabIndex = 4;
            this.TrainigPlayerButton.Text = "Trainings";
            this.TrainigPlayerButton.UseVisualStyleBackColor = true;
            this.TrainigPlayerButton.Click += new System.EventHandler(this.TrainigPlayerButton_Click);
            // 
            // EventsPlayerButton
            // 
            this.EventsPlayerButton.Location = new System.Drawing.Point(217, 213);
            this.EventsPlayerButton.Name = "EventsPlayerButton";
            this.EventsPlayerButton.Size = new System.Drawing.Size(75, 30);
            this.EventsPlayerButton.TabIndex = 4;
            this.EventsPlayerButton.Text = "Events";
            this.EventsPlayerButton.UseVisualStyleBackColor = true;
            this.EventsPlayerButton.Click += new System.EventHandler(this.EventsPlayerButton_Click);
            // 
            // dataGridViewPlayer
            // 
            this.dataGridViewPlayer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPlayer.Location = new System.Drawing.Point(217, 35);
            this.dataGridViewPlayer.Name = "dataGridViewPlayer";
            this.dataGridViewPlayer.RowTemplate.Height = 24;
            this.dataGridViewPlayer.Size = new System.Drawing.Size(851, 150);
            this.dataGridViewPlayer.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Player";
            // 
            // CoachPanel
            // 
            this.CoachPanel.Controls.Add(this.label55);
            this.CoachPanel.Controls.Add(this.gridPrenotazioniCoach);
            this.CoachPanel.Controls.Add(this.CollingButton);
            this.CoachPanel.Controls.Add(this.AddTrainingButton);
            this.CoachPanel.Controls.Add(this.DescriptionTrainingText);
            this.CoachPanel.Controls.Add(this.label31);
            this.CoachPanel.Controls.Add(this.label30);
            this.CoachPanel.Controls.Add(this.label29);
            this.CoachPanel.Controls.Add(this.label28);
            this.CoachPanel.Controls.Add(this.label27);
            this.CoachPanel.Controls.Add(this.PaceTrainingText);
            this.CoachPanel.Controls.Add(this.DurationTrainingText);
            this.CoachPanel.Controls.Add(this.TimeTrainingText);
            this.CoachPanel.Controls.Add(this.DateTrainingText);
            this.CoachPanel.Controls.Add(this.MatchesCoachButton);
            this.CoachPanel.Controls.Add(this.TrainigCoachButton);
            this.CoachPanel.Controls.Add(this.EventsCoachButton);
            this.CoachPanel.Controls.Add(this.dataGridViewCoach);
            this.CoachPanel.Controls.Add(this.label3);
            this.CoachPanel.Location = new System.Drawing.Point(37, 12);
            this.CoachPanel.Name = "CoachPanel";
            this.CoachPanel.Size = new System.Drawing.Size(1318, 630);
            this.CoachPanel.TabIndex = 4;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(923, 217);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(59, 17);
            this.label55.TabIndex = 39;
            this.label55.Text = "Booking";
            // 
            // gridPrenotazioniCoach
            // 
            this.gridPrenotazioniCoach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPrenotazioniCoach.Location = new System.Drawing.Point(744, 238);
            this.gridPrenotazioniCoach.Name = "gridPrenotazioniCoach";
            this.gridPrenotazioniCoach.RowTemplate.Height = 24;
            this.gridPrenotazioniCoach.Size = new System.Drawing.Size(505, 375);
            this.gridPrenotazioniCoach.TabIndex = 38;
            // 
            // CollingButton
            // 
            this.CollingButton.Location = new System.Drawing.Point(28, 90);
            this.CollingButton.Name = "CollingButton";
            this.CollingButton.Size = new System.Drawing.Size(119, 48);
            this.CollingButton.TabIndex = 15;
            this.CollingButton.Text = "Calling";
            this.CollingButton.UseVisualStyleBackColor = true;
            this.CollingButton.Click += new System.EventHandler(this.CollingButton_Click);
            // 
            // AddTrainingButton
            // 
            this.AddTrainingButton.Location = new System.Drawing.Point(257, 431);
            this.AddTrainingButton.Name = "AddTrainingButton";
            this.AddTrainingButton.Size = new System.Drawing.Size(167, 29);
            this.AddTrainingButton.TabIndex = 14;
            this.AddTrainingButton.Text = "AddTraining";
            this.AddTrainingButton.UseVisualStyleBackColor = true;
            this.AddTrainingButton.Click += new System.EventHandler(this.AddTrainingButton_Click);
            // 
            // DescriptionTrainingText
            // 
            this.DescriptionTrainingText.Location = new System.Drawing.Point(257, 319);
            this.DescriptionTrainingText.Name = "DescriptionTrainingText";
            this.DescriptionTrainingText.Size = new System.Drawing.Size(241, 96);
            this.DescriptionTrainingText.TabIndex = 13;
            this.DescriptionTrainingText.Text = "";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(254, 299);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(79, 17);
            this.label31.TabIndex = 12;
            this.label31.Text = "Description";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(15, 438);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(43, 17);
            this.label30.TabIndex = 11;
            this.label30.Text = "Place";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(15, 398);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(62, 17);
            this.label29.TabIndex = 10;
            this.label29.Text = "Duration";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(15, 349);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(39, 17);
            this.label28.TabIndex = 9;
            this.label28.Text = "Time";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(15, 299);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(38, 17);
            this.label27.TabIndex = 8;
            this.label27.Text = "Date";
            // 
            // PaceTrainingText
            // 
            this.PaceTrainingText.Location = new System.Drawing.Point(100, 438);
            this.PaceTrainingText.Name = "PaceTrainingText";
            this.PaceTrainingText.Size = new System.Drawing.Size(100, 22);
            this.PaceTrainingText.TabIndex = 7;
            // 
            // DurationTrainingText
            // 
            this.DurationTrainingText.Location = new System.Drawing.Point(100, 393);
            this.DurationTrainingText.Name = "DurationTrainingText";
            this.DurationTrainingText.Size = new System.Drawing.Size(100, 22);
            this.DurationTrainingText.TabIndex = 6;
            // 
            // TimeTrainingText
            // 
            this.TimeTrainingText.Location = new System.Drawing.Point(100, 345);
            this.TimeTrainingText.Name = "TimeTrainingText";
            this.TimeTrainingText.Size = new System.Drawing.Size(100, 22);
            this.TimeTrainingText.TabIndex = 5;
            // 
            // DateTrainingText
            // 
            this.DateTrainingText.Location = new System.Drawing.Point(100, 295);
            this.DateTrainingText.Name = "DateTrainingText";
            this.DateTrainingText.Size = new System.Drawing.Size(100, 22);
            this.DateTrainingText.TabIndex = 4;
            // 
            // MatchesCoachButton
            // 
            this.MatchesCoachButton.Location = new System.Drawing.Point(490, 235);
            this.MatchesCoachButton.Name = "MatchesCoachButton";
            this.MatchesCoachButton.Size = new System.Drawing.Size(75, 33);
            this.MatchesCoachButton.TabIndex = 3;
            this.MatchesCoachButton.Text = "Matches";
            this.MatchesCoachButton.UseVisualStyleBackColor = true;
            this.MatchesCoachButton.Click += new System.EventHandler(this.MatchesCoachButton_Click);
            // 
            // TrainigCoachButton
            // 
            this.TrainigCoachButton.Location = new System.Drawing.Point(368, 235);
            this.TrainigCoachButton.Name = "TrainigCoachButton";
            this.TrainigCoachButton.Size = new System.Drawing.Size(75, 33);
            this.TrainigCoachButton.TabIndex = 3;
            this.TrainigCoachButton.Text = "Trainings";
            this.TrainigCoachButton.UseVisualStyleBackColor = true;
            this.TrainigCoachButton.Click += new System.EventHandler(this.TrainigCoachButton_Click);
            // 
            // EventsCoachButton
            // 
            this.EventsCoachButton.Location = new System.Drawing.Point(236, 236);
            this.EventsCoachButton.Name = "EventsCoachButton";
            this.EventsCoachButton.Size = new System.Drawing.Size(75, 32);
            this.EventsCoachButton.TabIndex = 2;
            this.EventsCoachButton.Text = "Events";
            this.EventsCoachButton.UseVisualStyleBackColor = true;
            this.EventsCoachButton.Click += new System.EventHandler(this.EventsCoachButton_Click);
            // 
            // dataGridViewCoach
            // 
            this.dataGridViewCoach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCoach.Location = new System.Drawing.Point(236, 53);
            this.dataGridViewCoach.Name = "dataGridViewCoach";
            this.dataGridViewCoach.RowTemplate.Height = 24;
            this.dataGridViewCoach.Size = new System.Drawing.Size(782, 150);
            this.dataGridViewCoach.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(69, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Coach";
            // 
            // PrevButton
            // 
            this.PrevButton.Location = new System.Drawing.Point(48, 905);
            this.PrevButton.Name = "PrevButton";
            this.PrevButton.Size = new System.Drawing.Size(111, 30);
            this.PrevButton.TabIndex = 2;
            this.PrevButton.Text = "Back";
            this.PrevButton.UseVisualStyleBackColor = true;
            this.PrevButton.Click += new System.EventHandler(this.PrevButton_Click);
            // 
            // LoginButton
            // 
            this.LoginButton.Location = new System.Drawing.Point(259, 905);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(99, 30);
            this.LoginButton.TabIndex = 3;
            this.LoginButton.Text = "Login";
            this.LoginButton.UseVisualStyleBackColor = true;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // LeaderPanel
            // 
            this.LeaderPanel.Controls.Add(this.PlayerCategoryText);
            this.LeaderPanel.Controls.Add(this.AddChampionShipButton);
            this.LeaderPanel.Controls.Add(this.CategoryText);
            this.LeaderPanel.Controls.Add(this.StartingDateText);
            this.LeaderPanel.Controls.Add(this.label26);
            this.LeaderPanel.Controls.Add(this.label24);
            this.LeaderPanel.Controls.Add(this.AddCategoryButton);
            this.LeaderPanel.Controls.Add(this.AddPlayerButton);
            this.LeaderPanel.Controls.Add(this.AddCoachButton);
            this.LeaderPanel.Controls.Add(this.moreButton);
            this.LeaderPanel.Controls.Add(this.label20);
            this.LeaderPanel.Controls.Add(this.label18);
            this.LeaderPanel.Controls.Add(this.label14);
            this.LeaderPanel.Controls.Add(this.label33);
            this.LeaderPanel.Controls.Add(this.LastPaymentPlayerText);
            this.LeaderPanel.Controls.Add(this.label11);
            this.LeaderPanel.Controls.Add(this.label10);
            this.LeaderPanel.Controls.Add(this.LastMedicalCheckPlayerText);
            this.LeaderPanel.Controls.Add(this.PlayerRoleText);
            this.LeaderPanel.Controls.Add(this.label9);
            this.LeaderPanel.Controls.Add(this.label19);
            this.LeaderPanel.Controls.Add(this.label8);
            this.LeaderPanel.Controls.Add(this.label16);
            this.LeaderPanel.Controls.Add(this.label17);
            this.LeaderPanel.Controls.Add(this.label15);
            this.LeaderPanel.Controls.Add(this.label13);
            this.LeaderPanel.Controls.Add(this.label7);
            this.LeaderPanel.Controls.Add(this.label6);
            this.LeaderPanel.Controls.Add(this.label12);
            this.LeaderPanel.Controls.Add(this.label5);
            this.LeaderPanel.Controls.Add(this.CoachCFText);
            this.LeaderPanel.Controls.Add(this.PlayerCFText);
            this.LeaderPanel.Controls.Add(this.PaymentTextBox);
            this.LeaderPanel.Controls.Add(this.CoachSurnameText);
            this.LeaderPanel.Controls.Add(this.MaxAgeTextBox);
            this.LeaderPanel.Controls.Add(this.PlayerSurnameText);
            this.LeaderPanel.Controls.Add(this.MinAgeTextBox);
            this.LeaderPanel.Controls.Add(this.FirCardCoachText);
            this.LeaderPanel.Controls.Add(this.CoachNameText);
            this.LeaderPanel.Controls.Add(this.PlayerNameText);
            this.LeaderPanel.Controls.Add(this.FirCardPlayerText);
            this.LeaderPanel.Controls.Add(this.label4);
            this.LeaderPanel.Location = new System.Drawing.Point(19, 34);
            this.LeaderPanel.Name = "LeaderPanel";
            this.LeaderPanel.Size = new System.Drawing.Size(1459, 353);
            this.LeaderPanel.TabIndex = 5;
            // 
            // PlayerCategoryText
            // 
            this.PlayerCategoryText.Location = new System.Drawing.Point(1172, 74);
            this.PlayerCategoryText.Name = "PlayerCategoryText";
            this.PlayerCategoryText.Size = new System.Drawing.Size(90, 22);
            this.PlayerCategoryText.TabIndex = 32;
            // 
            // AddChampionShipButton
            // 
            this.AddChampionShipButton.Location = new System.Drawing.Point(1240, 261);
            this.AddChampionShipButton.Name = "AddChampionShipButton";
            this.AddChampionShipButton.Size = new System.Drawing.Size(194, 37);
            this.AddChampionShipButton.TabIndex = 31;
            this.AddChampionShipButton.Text = "AddChampionShip";
            this.AddChampionShipButton.UseVisualStyleBackColor = true;
            this.AddChampionShipButton.Click += new System.EventHandler(this.AddChampionShipButton_Click);
            // 
            // CategoryText
            // 
            this.CategoryText.Location = new System.Drawing.Point(355, 277);
            this.CategoryText.Name = "CategoryText";
            this.CategoryText.Size = new System.Drawing.Size(100, 22);
            this.CategoryText.TabIndex = 30;
            // 
            // StartingDateText
            // 
            this.StartingDateText.Location = new System.Drawing.Point(225, 277);
            this.StartingDateText.Name = "StartingDateText";
            this.StartingDateText.Size = new System.Drawing.Size(100, 22);
            this.StartingDateText.TabIndex = 28;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(355, 243);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 17);
            this.label26.TabIndex = 27;
            this.label26.Text = "Category";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(222, 243);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 17);
            this.label24.TabIndex = 25;
            this.label24.Text = "StartingYear";
            // 
            // AddCategoryButton
            // 
            this.AddCategoryButton.Location = new System.Drawing.Point(1307, 189);
            this.AddCategoryButton.Name = "AddCategoryButton";
            this.AddCategoryButton.Size = new System.Drawing.Size(127, 31);
            this.AddCategoryButton.TabIndex = 24;
            this.AddCategoryButton.Text = "AddCategory";
            this.AddCategoryButton.UseVisualStyleBackColor = true;
            this.AddCategoryButton.Click += new System.EventHandler(this.AddCategoryButton_Click);
            // 
            // AddPlayerButton
            // 
            this.AddPlayerButton.Location = new System.Drawing.Point(1342, 54);
            this.AddPlayerButton.Name = "AddPlayerButton";
            this.AddPlayerButton.Size = new System.Drawing.Size(92, 37);
            this.AddPlayerButton.TabIndex = 23;
            this.AddPlayerButton.Text = "AddPlayer";
            this.AddPlayerButton.UseVisualStyleBackColor = true;
            this.AddPlayerButton.Click += new System.EventHandler(this.AddPlayerButton_Click);
            // 
            // AddCoachButton
            // 
            this.AddCoachButton.Location = new System.Drawing.Point(1342, 118);
            this.AddCoachButton.Name = "AddCoachButton";
            this.AddCoachButton.Size = new System.Drawing.Size(92, 34);
            this.AddCoachButton.TabIndex = 22;
            this.AddCoachButton.Text = "AddCoach";
            this.AddCoachButton.UseVisualStyleBackColor = true;
            this.AddCoachButton.Click += new System.EventHandler(this.AddCoachButton_Click);
            // 
            // moreButton
            // 
            this.moreButton.Location = new System.Drawing.Point(34, 84);
            this.moreButton.Name = "moreButton";
            this.moreButton.Size = new System.Drawing.Size(91, 35);
            this.moreButton.TabIndex = 21;
            this.moreButton.Text = "more";
            this.moreButton.UseVisualStyleBackColor = true;
            this.moreButton.Click += new System.EventHandler(this.moreButton_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(495, 175);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 17);
            this.label20.TabIndex = 20;
            this.label20.Text = "Payment";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(355, 175);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 17);
            this.label18.TabIndex = 19;
            this.label18.Text = "MinAge";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(225, 175);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 17);
            this.label14.TabIndex = 18;
            this.label14.Text = "MaxAge";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(1169, 33);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(65, 17);
            this.label33.TabIndex = 16;
            this.label33.Text = "Category";
            // 
            // LastPaymentPlayerText
            // 
            this.LastPaymentPlayerText.Location = new System.Drawing.Point(1039, 74);
            this.LastPaymentPlayerText.Name = "LastPaymentPlayerText";
            this.LastPaymentPlayerText.Size = new System.Drawing.Size(100, 22);
            this.LastPaymentPlayerText.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1045, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 17);
            this.label11.TabIndex = 13;
            this.label11.Text = "Last Payment";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(900, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(130, 17);
            this.label10.TabIndex = 12;
            this.label10.Text = "Last Medical Check";
            // 
            // LastMedicalCheckPlayerText
            // 
            this.LastMedicalCheckPlayerText.Location = new System.Drawing.Point(903, 74);
            this.LastMedicalCheckPlayerText.Name = "LastMedicalCheckPlayerText";
            this.LastMedicalCheckPlayerText.Size = new System.Drawing.Size(97, 22);
            this.LastMedicalCheckPlayerText.TabIndex = 11;
            // 
            // PlayerRoleText
            // 
            this.PlayerRoleText.Location = new System.Drawing.Point(773, 74);
            this.PlayerRoleText.Name = "PlayerRoleText";
            this.PlayerRoleText.Size = new System.Drawing.Size(100, 22);
            this.PlayerRoleText.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(783, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 17);
            this.label9.TabIndex = 9;
            this.label9.Text = "Role";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(633, 102);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(25, 17);
            this.label19.TabIndex = 8;
            this.label19.Text = "CF";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(633, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "CF";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(352, 102);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 17);
            this.label16.TabIndex = 6;
            this.label16.Text = "Name";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(501, 102);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 17);
            this.label17.TabIndex = 7;
            this.label17.Text = "Surname";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(492, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 17);
            this.label15.TabIndex = 7;
            this.label15.Text = "Surname";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(352, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 17);
            this.label13.TabIndex = 6;
            this.label13.Text = "Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(492, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "Surname";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(352, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Name";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(222, 101);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 17);
            this.label12.TabIndex = 5;
            this.label12.Text = "Fir Card";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(222, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Fir Card";
            // 
            // CoachCFText
            // 
            this.CoachCFText.Location = new System.Drawing.Point(636, 130);
            this.CoachCFText.Name = "CoachCFText";
            this.CoachCFText.Size = new System.Drawing.Size(100, 22);
            this.CoachCFText.TabIndex = 4;
            // 
            // PlayerCFText
            // 
            this.PlayerCFText.Location = new System.Drawing.Point(636, 74);
            this.PlayerCFText.Name = "PlayerCFText";
            this.PlayerCFText.Size = new System.Drawing.Size(100, 22);
            this.PlayerCFText.TabIndex = 4;
            // 
            // PaymentTextBox
            // 
            this.PaymentTextBox.Location = new System.Drawing.Point(495, 198);
            this.PaymentTextBox.Name = "PaymentTextBox";
            this.PaymentTextBox.Size = new System.Drawing.Size(100, 22);
            this.PaymentTextBox.TabIndex = 3;
            // 
            // CoachSurnameText
            // 
            this.CoachSurnameText.Location = new System.Drawing.Point(495, 130);
            this.CoachSurnameText.Name = "CoachSurnameText";
            this.CoachSurnameText.Size = new System.Drawing.Size(100, 22);
            this.CoachSurnameText.TabIndex = 3;
            // 
            // MaxAgeTextBox
            // 
            this.MaxAgeTextBox.Location = new System.Drawing.Point(225, 198);
            this.MaxAgeTextBox.Name = "MaxAgeTextBox";
            this.MaxAgeTextBox.Size = new System.Drawing.Size(100, 22);
            this.MaxAgeTextBox.TabIndex = 1;
            // 
            // PlayerSurnameText
            // 
            this.PlayerSurnameText.Location = new System.Drawing.Point(495, 74);
            this.PlayerSurnameText.Name = "PlayerSurnameText";
            this.PlayerSurnameText.Size = new System.Drawing.Size(100, 22);
            this.PlayerSurnameText.TabIndex = 3;
            // 
            // MinAgeTextBox
            // 
            this.MinAgeTextBox.Location = new System.Drawing.Point(355, 198);
            this.MinAgeTextBox.Name = "MinAgeTextBox";
            this.MinAgeTextBox.Size = new System.Drawing.Size(100, 22);
            this.MinAgeTextBox.TabIndex = 2;
            // 
            // FirCardCoachText
            // 
            this.FirCardCoachText.Location = new System.Drawing.Point(225, 130);
            this.FirCardCoachText.Name = "FirCardCoachText";
            this.FirCardCoachText.Size = new System.Drawing.Size(100, 22);
            this.FirCardCoachText.TabIndex = 1;
            // 
            // CoachNameText
            // 
            this.CoachNameText.Location = new System.Drawing.Point(355, 130);
            this.CoachNameText.Name = "CoachNameText";
            this.CoachNameText.Size = new System.Drawing.Size(100, 22);
            this.CoachNameText.TabIndex = 2;
            // 
            // PlayerNameText
            // 
            this.PlayerNameText.Location = new System.Drawing.Point(355, 74);
            this.PlayerNameText.Name = "PlayerNameText";
            this.PlayerNameText.Size = new System.Drawing.Size(100, 22);
            this.PlayerNameText.TabIndex = 2;
            // 
            // FirCardPlayerText
            // 
            this.FirCardPlayerText.Location = new System.Drawing.Point(225, 74);
            this.FirCardPlayerText.Name = "FirCardPlayerText";
            this.FirCardPlayerText.Size = new System.Drawing.Size(100, 22);
            this.FirCardPlayerText.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "LeaderPanel";
            // 
            // MoreLeaderPanel
            // 
            this.MoreLeaderPanel.Controls.Add(this.label54);
            this.MoreLeaderPanel.Controls.Add(this.gridPrenotazioniLeader);
            this.MoreLeaderPanel.Controls.Add(this.AddEventButton);
            this.MoreLeaderPanel.Controls.Add(this.ResponsabileTX);
            this.MoreLeaderPanel.Controls.Add(this.label53);
            this.MoreLeaderPanel.Controls.Add(this.label52);
            this.MoreLeaderPanel.Controls.Add(this.label51);
            this.MoreLeaderPanel.Controls.Add(this.label50);
            this.MoreLeaderPanel.Controls.Add(this.label49);
            this.MoreLeaderPanel.Controls.Add(this.label44);
            this.MoreLeaderPanel.Controls.Add(this.NomeEventoTX);
            this.MoreLeaderPanel.Controls.Add(this.DurataEventTX);
            this.MoreLeaderPanel.Controls.Add(this.SpazioEventsTX);
            this.MoreLeaderPanel.Controls.Add(this.DataEventTX);
            this.MoreLeaderPanel.Controls.Add(this.TempoEventsTX);
            this.MoreLeaderPanel.Controls.Add(this.InsertMatchButton);
            this.MoreLeaderPanel.Controls.Add(this.label40);
            this.MoreLeaderPanel.Controls.Add(this.homeBoxMatch);
            this.MoreLeaderPanel.Controls.Add(this.avversariaTextMatch);
            this.MoreLeaderPanel.Controls.Add(this.tempoTextMatch);
            this.MoreLeaderPanel.Controls.Add(this.dateTextMatch);
            this.MoreLeaderPanel.Controls.Add(this.categoryTextMatch);
            this.MoreLeaderPanel.Controls.Add(this.seasonTextMatch);
            this.MoreLeaderPanel.Controls.Add(this.label39);
            this.MoreLeaderPanel.Controls.Add(this.label38);
            this.MoreLeaderPanel.Controls.Add(this.label37);
            this.MoreLeaderPanel.Controls.Add(this.label36);
            this.MoreLeaderPanel.Controls.Add(this.label35);
            this.MoreLeaderPanel.Controls.Add(this.ShowChampionShipButton);
            this.MoreLeaderPanel.Controls.Add(this.CategoryAndCoachButton);
            this.MoreLeaderPanel.Controls.Add(this.AssignButton);
            this.MoreLeaderPanel.Controls.Add(this.CategoryAssigText);
            this.MoreLeaderPanel.Controls.Add(this.CoachFirCardAssingText);
            this.MoreLeaderPanel.Controls.Add(this.label23);
            this.MoreLeaderPanel.Controls.Add(this.label22);
            this.MoreLeaderPanel.Controls.Add(this.WhoNeedMedicalCheckButton);
            this.MoreLeaderPanel.Controls.Add(this.WhoHaventPayButton);
            this.MoreLeaderPanel.Controls.Add(this.dataGridViewLeaderMore);
            this.MoreLeaderPanel.Location = new System.Drawing.Point(12, 3);
            this.MoreLeaderPanel.Name = "MoreLeaderPanel";
            this.MoreLeaderPanel.Size = new System.Drawing.Size(2058, 613);
            this.MoreLeaderPanel.TabIndex = 6;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(1244, 203);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(59, 17);
            this.label54.TabIndex = 37;
            this.label54.Text = "Booking";
            // 
            // gridPrenotazioniLeader
            // 
            this.gridPrenotazioniLeader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPrenotazioniLeader.Location = new System.Drawing.Point(1247, 236);
            this.gridPrenotazioniLeader.Name = "gridPrenotazioniLeader";
            this.gridPrenotazioniLeader.RowTemplate.Height = 24;
            this.gridPrenotazioniLeader.Size = new System.Drawing.Size(477, 344);
            this.gridPrenotazioniLeader.TabIndex = 36;
            // 
            // AddEventButton
            // 
            this.AddEventButton.Location = new System.Drawing.Point(950, 321);
            this.AddEventButton.Name = "AddEventButton";
            this.AddEventButton.Size = new System.Drawing.Size(143, 63);
            this.AddEventButton.TabIndex = 35;
            this.AddEventButton.Text = "AddEvent";
            this.AddEventButton.UseVisualStyleBackColor = true;
            this.AddEventButton.Click += new System.EventHandler(this.AddEventButton_Click);
            // 
            // ResponsabileTX
            // 
            this.ResponsabileTX.Location = new System.Drawing.Point(799, 525);
            this.ResponsabileTX.Name = "ResponsabileTX";
            this.ResponsabileTX.Size = new System.Drawing.Size(100, 22);
            this.ResponsabileTX.TabIndex = 34;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(697, 525);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(86, 17);
            this.label53.TabIndex = 33;
            this.label53.Text = "Responsible";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(721, 492);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(51, 17);
            this.label52.TabIndex = 32;
            this.label52.Text = "Events";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(721, 454);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(62, 17);
            this.label51.TabIndex = 31;
            this.label51.Text = "Duration";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(718, 410);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(48, 17);
            this.label50.TabIndex = 30;
            this.label50.Text = "Space";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(718, 365);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(39, 17);
            this.label49.TabIndex = 29;
            this.label49.Text = "Time";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(718, 326);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(38, 17);
            this.label44.TabIndex = 28;
            this.label44.Text = "Date";
            // 
            // NomeEventoTX
            // 
            this.NomeEventoTX.Location = new System.Drawing.Point(799, 489);
            this.NomeEventoTX.Name = "NomeEventoTX";
            this.NomeEventoTX.Size = new System.Drawing.Size(100, 22);
            this.NomeEventoTX.TabIndex = 27;
            // 
            // DurataEventTX
            // 
            this.DurataEventTX.Location = new System.Drawing.Point(799, 450);
            this.DurataEventTX.Name = "DurataEventTX";
            this.DurataEventTX.Size = new System.Drawing.Size(100, 22);
            this.DurataEventTX.TabIndex = 26;
            // 
            // SpazioEventsTX
            // 
            this.SpazioEventsTX.Location = new System.Drawing.Point(799, 410);
            this.SpazioEventsTX.Name = "SpazioEventsTX";
            this.SpazioEventsTX.Size = new System.Drawing.Size(100, 22);
            this.SpazioEventsTX.TabIndex = 25;
            // 
            // DataEventTX
            // 
            this.DataEventTX.Location = new System.Drawing.Point(799, 321);
            this.DataEventTX.Name = "DataEventTX";
            this.DataEventTX.Size = new System.Drawing.Size(100, 22);
            this.DataEventTX.TabIndex = 24;
            // 
            // TempoEventsTX
            // 
            this.TempoEventsTX.Location = new System.Drawing.Point(799, 362);
            this.TempoEventsTX.Name = "TempoEventsTX";
            this.TempoEventsTX.Size = new System.Drawing.Size(100, 22);
            this.TempoEventsTX.TabIndex = 23;
            // 
            // InsertMatchButton
            // 
            this.InsertMatchButton.Location = new System.Drawing.Point(371, 514);
            this.InsertMatchButton.Name = "InsertMatchButton";
            this.InsertMatchButton.Size = new System.Drawing.Size(122, 46);
            this.InsertMatchButton.TabIndex = 22;
            this.InsertMatchButton.Text = "InsertMatch";
            this.InsertMatchButton.UseVisualStyleBackColor = true;
            this.InsertMatchButton.Click += new System.EventHandler(this.InsertMatchButton_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(86, 539);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(61, 17);
            this.label40.TabIndex = 21;
            this.label40.Text = "at Home";
            // 
            // homeBoxMatch
            // 
            this.homeBoxMatch.AutoSize = true;
            this.homeBoxMatch.Location = new System.Drawing.Point(190, 539);
            this.homeBoxMatch.Name = "homeBoxMatch";
            this.homeBoxMatch.Size = new System.Drawing.Size(53, 21);
            this.homeBoxMatch.TabIndex = 20;
            this.homeBoxMatch.Text = "Y/N";
            this.homeBoxMatch.UseVisualStyleBackColor = true;
            // 
            // avversariaTextMatch
            // 
            this.avversariaTextMatch.Location = new System.Drawing.Point(190, 511);
            this.avversariaTextMatch.Name = "avversariaTextMatch";
            this.avversariaTextMatch.Size = new System.Drawing.Size(100, 22);
            this.avversariaTextMatch.TabIndex = 19;
            // 
            // tempoTextMatch
            // 
            this.tempoTextMatch.Location = new System.Drawing.Point(190, 483);
            this.tempoTextMatch.Name = "tempoTextMatch";
            this.tempoTextMatch.Size = new System.Drawing.Size(100, 22);
            this.tempoTextMatch.TabIndex = 18;
            // 
            // dateTextMatch
            // 
            this.dateTextMatch.Location = new System.Drawing.Point(190, 455);
            this.dateTextMatch.Name = "dateTextMatch";
            this.dateTextMatch.Size = new System.Drawing.Size(100, 22);
            this.dateTextMatch.TabIndex = 17;
            // 
            // categoryTextMatch
            // 
            this.categoryTextMatch.Location = new System.Drawing.Point(190, 426);
            this.categoryTextMatch.Name = "categoryTextMatch";
            this.categoryTextMatch.Size = new System.Drawing.Size(100, 22);
            this.categoryTextMatch.TabIndex = 16;
            // 
            // seasonTextMatch
            // 
            this.seasonTextMatch.Location = new System.Drawing.Point(190, 399);
            this.seasonTextMatch.Name = "seasonTextMatch";
            this.seasonTextMatch.Size = new System.Drawing.Size(100, 22);
            this.seasonTextMatch.TabIndex = 15;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(86, 511);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(51, 17);
            this.label39.TabIndex = 14;
            this.label39.Text = "Enemy";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(86, 483);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(39, 17);
            this.label38.TabIndex = 13;
            this.label38.Text = "Time";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(86, 455);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(38, 17);
            this.label37.TabIndex = 12;
            this.label37.Text = "Date";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(86, 431);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(65, 17);
            this.label36.TabIndex = 11;
            this.label36.Text = "Category";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(86, 404);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(56, 17);
            this.label35.TabIndex = 10;
            this.label35.Text = "Season";
            // 
            // ShowChampionShipButton
            // 
            this.ShowChampionShipButton.Location = new System.Drawing.Point(1148, 115);
            this.ShowChampionShipButton.Name = "ShowChampionShipButton";
            this.ShowChampionShipButton.Size = new System.Drawing.Size(221, 51);
            this.ShowChampionShipButton.TabIndex = 9;
            this.ShowChampionShipButton.Text = "ShowChampionShips";
            this.ShowChampionShipButton.UseVisualStyleBackColor = true;
            this.ShowChampionShipButton.Click += new System.EventHandler(this.ShowChampionShipButton_Click);
            // 
            // CategoryAndCoachButton
            // 
            this.CategoryAndCoachButton.Location = new System.Drawing.Point(1148, 37);
            this.CategoryAndCoachButton.Name = "CategoryAndCoachButton";
            this.CategoryAndCoachButton.Size = new System.Drawing.Size(221, 50);
            this.CategoryAndCoachButton.TabIndex = 8;
            this.CategoryAndCoachButton.Text = "CategoriesAndCoaches";
            this.CategoryAndCoachButton.UseVisualStyleBackColor = true;
            this.CategoryAndCoachButton.Click += new System.EventHandler(this.CategoryAndCoachButton_Click);
            // 
            // AssignButton
            // 
            this.AssignButton.Location = new System.Drawing.Point(397, 319);
            this.AssignButton.Name = "AssignButton";
            this.AssignButton.Size = new System.Drawing.Size(75, 39);
            this.AssignButton.TabIndex = 7;
            this.AssignButton.Text = "Assign";
            this.AssignButton.UseVisualStyleBackColor = true;
            this.AssignButton.Click += new System.EventHandler(this.AssignButton_Click);
            // 
            // CategoryAssigText
            // 
            this.CategoryAssigText.Location = new System.Drawing.Point(241, 336);
            this.CategoryAssigText.Name = "CategoryAssigText";
            this.CategoryAssigText.Size = new System.Drawing.Size(100, 22);
            this.CategoryAssigText.TabIndex = 6;
            // 
            // CoachFirCardAssingText
            // 
            this.CoachFirCardAssingText.Location = new System.Drawing.Point(81, 336);
            this.CoachFirCardAssingText.Name = "CoachFirCardAssingText";
            this.CoachFirCardAssingText.Size = new System.Drawing.Size(100, 22);
            this.CoachFirCardAssingText.TabIndex = 5;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(238, 298);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 17);
            this.label23.TabIndex = 4;
            this.label23.Text = "Category";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(78, 298);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 17);
            this.label22.TabIndex = 3;
            this.label22.Text = "FirCardCoach";
            // 
            // WhoNeedMedicalCheckButton
            // 
            this.WhoNeedMedicalCheckButton.Location = new System.Drawing.Point(873, 115);
            this.WhoNeedMedicalCheckButton.Name = "WhoNeedMedicalCheckButton";
            this.WhoNeedMedicalCheckButton.Size = new System.Drawing.Size(221, 51);
            this.WhoNeedMedicalCheckButton.TabIndex = 2;
            this.WhoNeedMedicalCheckButton.Text = "WhoNeedMedicalCheck";
            this.WhoNeedMedicalCheckButton.UseVisualStyleBackColor = true;
            this.WhoNeedMedicalCheckButton.Click += new System.EventHandler(this.WhoNeedMedicalCheckButton_Click);
            // 
            // WhoHaventPayButton
            // 
            this.WhoHaventPayButton.Location = new System.Drawing.Point(873, 37);
            this.WhoHaventPayButton.Name = "WhoHaventPayButton";
            this.WhoHaventPayButton.Size = new System.Drawing.Size(221, 50);
            this.WhoHaventPayButton.TabIndex = 1;
            this.WhoHaventPayButton.Text = "WhoHaven\'tPay";
            this.WhoHaventPayButton.UseVisualStyleBackColor = true;
            this.WhoHaventPayButton.Click += new System.EventHandler(this.WhoHaventPayButton_Click);
            // 
            // dataGridViewLeaderMore
            // 
            this.dataGridViewLeaderMore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLeaderMore.Location = new System.Drawing.Point(34, 37);
            this.dataGridViewLeaderMore.Name = "dataGridViewLeaderMore";
            this.dataGridViewLeaderMore.RowTemplate.Height = 24;
            this.dataGridViewLeaderMore.Size = new System.Drawing.Size(786, 233);
            this.dataGridViewLeaderMore.TabIndex = 0;
            // 
            // CallingPanel
            // 
            this.CallingPanel.Controls.Add(this.label48);
            this.CallingPanel.Controls.Add(this.data_fine);
            this.CallingPanel.Controls.Add(this.AddEpisodeButton);
            this.CallingPanel.Controls.Add(this.EventText);
            this.CallingPanel.Controls.Add(this.SecondEventText);
            this.CallingPanel.Controls.Add(this.PlayerFirCardEventText);
            this.CallingPanel.Controls.Add(this.DateEventsText);
            this.CallingPanel.Controls.Add(this.SeasonEventsText);
            this.CallingPanel.Controls.Add(this.label47);
            this.CallingPanel.Controls.Add(this.label46);
            this.CallingPanel.Controls.Add(this.label45);
            this.CallingPanel.Controls.Add(this.label43);
            this.CallingPanel.Controls.Add(this.label42);
            this.CallingPanel.Controls.Add(this.StatisticGridCoach);
            this.CallingPanel.Controls.Add(this.PlayerStatisticCoach);
            this.CallingPanel.Controls.Add(this.label41);
            this.CallingPanel.Controls.Add(this.PlayerCardText);
            this.CallingPanel.Controls.Add(this.label34);
            this.CallingPanel.Controls.Add(this.label25);
            this.CallingPanel.Controls.Add(this.textBoxPlayerCalling);
            this.CallingPanel.Controls.Add(this.textBoxMatchCalling);
            this.CallingPanel.Controls.Add(this.CallingButton);
            this.CallingPanel.Controls.Add(this.label21);
            this.CallingPanel.Controls.Add(this.Matches);
            this.CallingPanel.Controls.Add(this.gridPlayerCallingView);
            this.CallingPanel.Controls.Add(this.gridMatchesCallingView);
            this.CallingPanel.Controls.Add(this.label32);
            this.CallingPanel.Location = new System.Drawing.Point(1, 15);
            this.CallingPanel.Name = "CallingPanel";
            this.CallingPanel.Size = new System.Drawing.Size(1370, 807);
            this.CallingPanel.TabIndex = 2;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(278, 694);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(79, 17);
            this.label48.TabIndex = 44;
            this.label48.Text = "Date_finish";
            // 
            // data_fine
            // 
            this.data_fine.Location = new System.Drawing.Point(278, 723);
            this.data_fine.Name = "data_fine";
            this.data_fine.Size = new System.Drawing.Size(100, 22);
            this.data_fine.TabIndex = 43;
            // 
            // AddEpisodeButton
            // 
            this.AddEpisodeButton.Location = new System.Drawing.Point(833, 714);
            this.AddEpisodeButton.Name = "AddEpisodeButton";
            this.AddEpisodeButton.Size = new System.Drawing.Size(148, 41);
            this.AddEpisodeButton.TabIndex = 42;
            this.AddEpisodeButton.Text = "AddEpisode";
            this.AddEpisodeButton.UseVisualStyleBackColor = true;
            this.AddEpisodeButton.Click += new System.EventHandler(this.AddEpisodeButton_Click);
            // 
            // EventText
            // 
            this.EventText.Location = new System.Drawing.Point(670, 723);
            this.EventText.Name = "EventText";
            this.EventText.Size = new System.Drawing.Size(100, 22);
            this.EventText.TabIndex = 41;
            // 
            // SecondEventText
            // 
            this.SecondEventText.Location = new System.Drawing.Point(532, 723);
            this.SecondEventText.Name = "SecondEventText";
            this.SecondEventText.Size = new System.Drawing.Size(100, 22);
            this.SecondEventText.TabIndex = 40;
            // 
            // PlayerFirCardEventText
            // 
            this.PlayerFirCardEventText.Location = new System.Drawing.Point(408, 723);
            this.PlayerFirCardEventText.Name = "PlayerFirCardEventText";
            this.PlayerFirCardEventText.Size = new System.Drawing.Size(100, 22);
            this.PlayerFirCardEventText.TabIndex = 39;
            // 
            // DateEventsText
            // 
            this.DateEventsText.Location = new System.Drawing.Point(162, 723);
            this.DateEventsText.Name = "DateEventsText";
            this.DateEventsText.Size = new System.Drawing.Size(100, 22);
            this.DateEventsText.TabIndex = 37;
            // 
            // SeasonEventsText
            // 
            this.SeasonEventsText.Location = new System.Drawing.Point(46, 723);
            this.SeasonEventsText.Name = "SeasonEventsText";
            this.SeasonEventsText.Size = new System.Drawing.Size(100, 22);
            this.SeasonEventsText.TabIndex = 36;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(682, 694);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(51, 17);
            this.label47.TabIndex = 35;
            this.label47.Text = "Events";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(556, 694);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(56, 17);
            this.label46.TabIndex = 34;
            this.label46.Text = "Second";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(413, 694);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(94, 17);
            this.label45.TabIndex = 33;
            this.label45.Text = "PlayerFirCard";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(185, 694);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(38, 17);
            this.label43.TabIndex = 31;
            this.label43.Text = "Date";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(65, 694);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(56, 17);
            this.label42.TabIndex = 30;
            this.label42.Text = "Season";
            // 
            // StatisticGridCoach
            // 
            this.StatisticGridCoach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StatisticGridCoach.Location = new System.Drawing.Point(39, 506);
            this.StatisticGridCoach.Name = "StatisticGridCoach";
            this.StatisticGridCoach.RowTemplate.Height = 24;
            this.StatisticGridCoach.Size = new System.Drawing.Size(810, 150);
            this.StatisticGridCoach.TabIndex = 29;
            // 
            // PlayerStatisticCoach
            // 
            this.PlayerStatisticCoach.Location = new System.Drawing.Point(1173, 565);
            this.PlayerStatisticCoach.Name = "PlayerStatisticCoach";
            this.PlayerStatisticCoach.Size = new System.Drawing.Size(75, 23);
            this.PlayerStatisticCoach.TabIndex = 28;
            this.PlayerStatisticCoach.Text = "Statistics";
            this.PlayerStatisticCoach.UseVisualStyleBackColor = true;
            this.PlayerStatisticCoach.Click += new System.EventHandler(this.PlayerStatisticCoach_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(870, 571);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(78, 17);
            this.label41.TabIndex = 27;
            this.label41.Text = "PlayerCard";
            // 
            // PlayerCardText
            // 
            this.PlayerCardText.Location = new System.Drawing.Point(969, 566);
            this.PlayerCardText.Name = "PlayerCardText";
            this.PlayerCardText.Size = new System.Drawing.Size(155, 22);
            this.PlayerCardText.TabIndex = 26;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(126, 395);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(48, 17);
            this.label34.TabIndex = 10;
            this.label34.Text = "Player";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(123, 346);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(50, 17);
            this.label25.TabIndex = 9;
            this.label25.Text = "Match:";
            // 
            // textBoxPlayerCalling
            // 
            this.textBoxPlayerCalling.Location = new System.Drawing.Point(228, 391);
            this.textBoxPlayerCalling.Name = "textBoxPlayerCalling";
            this.textBoxPlayerCalling.Size = new System.Drawing.Size(100, 22);
            this.textBoxPlayerCalling.TabIndex = 8;
            // 
            // textBoxMatchCalling
            // 
            this.textBoxMatchCalling.Location = new System.Drawing.Point(228, 341);
            this.textBoxMatchCalling.Name = "textBoxMatchCalling";
            this.textBoxMatchCalling.Size = new System.Drawing.Size(100, 22);
            this.textBoxMatchCalling.TabIndex = 7;
            // 
            // CallingButton
            // 
            this.CallingButton.Location = new System.Drawing.Point(370, 341);
            this.CallingButton.Name = "CallingButton";
            this.CallingButton.Size = new System.Drawing.Size(75, 23);
            this.CallingButton.TabIndex = 6;
            this.CallingButton.Text = "Call";
            this.CallingButton.UseVisualStyleBackColor = true;
            this.CallingButton.Click += new System.EventHandler(this.CallingButton_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(932, 99);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(55, 17);
            this.label21.TabIndex = 5;
            this.label21.Text = "Players";
            // 
            // Matches
            // 
            this.Matches.AutoSize = true;
            this.Matches.Location = new System.Drawing.Point(39, 83);
            this.Matches.Name = "Matches";
            this.Matches.Size = new System.Drawing.Size(61, 17);
            this.Matches.TabIndex = 4;
            this.Matches.Text = "Matches";
            // 
            // gridPlayerCallingView
            // 
            this.gridPlayerCallingView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPlayerCallingView.Location = new System.Drawing.Point(873, 127);
            this.gridPlayerCallingView.Name = "gridPlayerCallingView";
            this.gridPlayerCallingView.RowTemplate.Height = 24;
            this.gridPlayerCallingView.Size = new System.Drawing.Size(488, 348);
            this.gridPlayerCallingView.TabIndex = 3;
            // 
            // gridMatchesCallingView
            // 
            this.gridMatchesCallingView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMatchesCallingView.Location = new System.Drawing.Point(39, 127);
            this.gridMatchesCallingView.Name = "gridMatchesCallingView";
            this.gridMatchesCallingView.RowTemplate.Height = 24;
            this.gridMatchesCallingView.Size = new System.Drawing.Size(813, 190);
            this.gridMatchesCallingView.TabIndex = 1;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(36, 22);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(57, 17);
            this.label32.TabIndex = 0;
            this.label32.Text = "Callings";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 1055);
            this.Controls.Add(this.CallingPanel);
            this.Controls.Add(this.MoreLeaderPanel);
            this.Controls.Add(this.LeaderPanel);
            this.Controls.Add(this.CoachPanel);
            this.Controls.Add(this.LoginButton);
            this.Controls.Add(this.PrevButton);
            this.Controls.Add(this.PanelPlayer);
            this.Controls.Add(this.PanelLogin);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RugbyForlì";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.PanelLogin.ResumeLayout(false);
            this.PanelLogin.PerformLayout();
            this.PanelPlayer.ResumeLayout(false);
            this.PanelPlayer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlayer)).EndInit();
            this.CoachPanel.ResumeLayout(false);
            this.CoachPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPrenotazioniCoach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCoach)).EndInit();
            this.LeaderPanel.ResumeLayout(false);
            this.LeaderPanel.PerformLayout();
            this.MoreLeaderPanel.ResumeLayout(false);
            this.MoreLeaderPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPrenotazioniLeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLeaderMore)).EndInit();
            this.CallingPanel.ResumeLayout(false);
            this.CallingPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatisticGridCoach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPlayerCallingView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMatchesCallingView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelLogin;
        private System.Windows.Forms.Button PlayerButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel PanelPlayer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button PrevButton;
        private System.Windows.Forms.Button LoginButton;
        private System.Windows.Forms.Button LeaderButton;
        private System.Windows.Forms.Button CoachButton;
        private System.Windows.Forms.TextBox LoadingTextBox;
        private System.Windows.Forms.DataGridView dataGridViewPlayer;
        private System.Windows.Forms.Button MatchesPlayerButton;
        private System.Windows.Forms.Button TrainigPlayerButton;
        private System.Windows.Forms.Button EventsPlayerButton;
        private System.Windows.Forms.Panel CoachPanel;
        private System.Windows.Forms.Button MatchesCoachButton;
        private System.Windows.Forms.Button TrainigCoachButton;
        private System.Windows.Forms.Button EventsCoachButton;
        private System.Windows.Forms.DataGridView dataGridViewCoach;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel LeaderPanel;
        private System.Windows.Forms.TextBox LastPaymentPlayerText;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox LastMedicalCheckPlayerText;
        private System.Windows.Forms.TextBox PlayerRoleText;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox CoachCFText;
        private System.Windows.Forms.TextBox PlayerCFText;
        private System.Windows.Forms.TextBox CoachSurnameText;
        private System.Windows.Forms.TextBox PlayerSurnameText;
        private System.Windows.Forms.TextBox FirCardCoachText;
        private System.Windows.Forms.TextBox CoachNameText;
        private System.Windows.Forms.TextBox PlayerNameText;
        private System.Windows.Forms.TextBox FirCardPlayerText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox PaymentTextBox;
        private System.Windows.Forms.TextBox MaxAgeTextBox;
        private System.Windows.Forms.TextBox MinAgeTextBox;
        private System.Windows.Forms.Button moreButton;
        private System.Windows.Forms.Button AddCategoryButton;
        private System.Windows.Forms.Button AddPlayerButton;
        private System.Windows.Forms.Button AddCoachButton;
        private System.Windows.Forms.Panel MoreLeaderPanel;
        private System.Windows.Forms.Button WhoHaventPayButton;
        private System.Windows.Forms.DataGridView dataGridViewLeaderMore;
        private System.Windows.Forms.Button WhoNeedMedicalCheckButton;
        private System.Windows.Forms.Button AssignButton;
        private System.Windows.Forms.TextBox CategoryAssigText;
        private System.Windows.Forms.TextBox CoachFirCardAssingText;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button CategoryAndCoachButton;
        private System.Windows.Forms.TextBox StartingDateText;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button AddChampionShipButton;
        private System.Windows.Forms.TextBox CategoryText;
        private System.Windows.Forms.Button ShowChampionShipButton;
        private System.Windows.Forms.Button AddTrainingButton;
        private System.Windows.Forms.RichTextBox DescriptionTrainingText;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox PaceTrainingText;
        private System.Windows.Forms.TextBox DurationTrainingText;
        private System.Windows.Forms.TextBox TimeTrainingText;
        private System.Windows.Forms.TextBox DateTrainingText;
        private System.Windows.Forms.Button CollingButton;
        private System.Windows.Forms.Panel CallingPanel;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox PlayerCategoryText;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label Matches;
        private System.Windows.Forms.DataGridView gridPlayerCallingView;
        private System.Windows.Forms.DataGridView gridMatchesCallingView;
        private System.Windows.Forms.Button CallingButton;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBoxPlayerCalling;
        private System.Windows.Forms.TextBox textBoxMatchCalling;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.CheckBox homeBoxMatch;
        private System.Windows.Forms.TextBox avversariaTextMatch;
        private System.Windows.Forms.TextBox tempoTextMatch;
        private System.Windows.Forms.TextBox dateTextMatch;
        private System.Windows.Forms.TextBox categoryTextMatch;
        private System.Windows.Forms.TextBox seasonTextMatch;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button InsertMatchButton;
        private System.Windows.Forms.DataGridView StatisticGridCoach;
        private System.Windows.Forms.Button PlayerStatisticCoach;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox PlayerCardText;
        private System.Windows.Forms.Button StatisticButtonPlayer;
        private System.Windows.Forms.TextBox EventText;
        private System.Windows.Forms.TextBox SecondEventText;
        private System.Windows.Forms.TextBox PlayerFirCardEventText;
        private System.Windows.Forms.TextBox DateEventsText;
        private System.Windows.Forms.TextBox SeasonEventsText;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button AddEpisodeButton;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox data_fine;
        private System.Windows.Forms.Button AddEventButton;
        private System.Windows.Forms.TextBox ResponsabileTX;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox NomeEventoTX;
        private System.Windows.Forms.TextBox DurataEventTX;
        private System.Windows.Forms.TextBox SpazioEventsTX;
        private System.Windows.Forms.TextBox DataEventTX;
        private System.Windows.Forms.TextBox TempoEventsTX;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.DataGridView gridPrenotazioniLeader;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.DataGridView gridPrenotazioniCoach;
    }
}