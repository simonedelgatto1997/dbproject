﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Linq;


namespace DBProject
{
    class ModelImpl : Model
    {
        private DataBaseDataContext db = new DataBaseDataContext();
        private GIOCATORI CurrentPlayer;
        private ALLENATORI CurrentCoach;
        private static string CONVOCAZIONI_STRING = "Controllare di aver inserito correttamente il codice della partita e quello del giocatore o di non aver già effettuato la convocazione";
        private static string ADDALLENATORE_STRING = "Controllare di non aver già inserito l'allenatore precedentemente";
        private static string GIOCATORE_STRING = "Controllare di non aver già inserito il giocatore precedentemente";
        private static string ADDALLENAMENTI_STRING = "Controllare che non sia già stato inserito l'allenamento o che non ci siano sovrapposizioni con altri eventi";
        private static string NON_E_IL_TUO_GIOCATORE = "Il giocatore non fa parte di quelli della rosa gestiti dal questo allenatore";
        private static string CHECK_CATEGORIA = "La categoria o esiste già oppure controllare l'Età massima e l'Età minima inserita";
        private static string ADD_EVENT_ERROR = "Forse l'evento è gia stato inserito oppure i parametri non sono corretti";

        public void AddCategory(int maxAge, int minAge, int cost)
        {
            try
            {
                CATEGORIE c = new CATEGORIE
                {
                    Eta_min = minAge,
                    quota_Iscrizione = cost,
                    Eta_max = maxAge
                };
                db.CATEGORIE.InsertOnSubmit(c);
                db.SubmitChanges();
            } catch (Exception e)
            {
                DiscardPendingChanges();
                throw new Exception(CHECK_CATEGORIA);
            }
               

        }

        public void checkCoachLogin(String tesseraFir)
        {
            var coach = (from g in db.ALLENATORI
                          where g.Cod_tessera_Fir_Allenatore == tesseraFir
                          select g).First();

            if (coach != null) this.CurrentCoach = coach;
            
        }

        public void checkLeaderLogin(String password)
        {
            if (!password.Equals("leader"))
            {
                throw new Exception("Password not correct");
            }
        }
        
        public void AddNewChampionship(string annoInizio, string Categoria)
        {
            string cat = exstractCategory(Categoria);
            try
            {
                CAMPIONATI c = new CAMPIONATI
                {
                    Categoria = Int32.Parse(cat),
                    Anno_Inizio = Int32.Parse(annoInizio)

                };
                db.CAMPIONATI.InsertOnSubmit(c);
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                DiscardPendingChanges();
                throw e;
            }
           
        }

        public void AssingCoach(string tesseraFir, string Categoria)
        {
            Categoria = exstractCategory(Categoria);
            try
            {
                ASSEGNAMENTI_CATEGORIE_ALLENATORI ac = new ASSEGNAMENTI_CATEGORIE_ALLENATORI
                {
                    Allenatore = tesseraFir,
                    Categoria = Int32.Parse(Categoria)
                };
                db.ASSEGNAMENTI_CATEGORIE_ALLENATORI.InsertOnSubmit(ac);
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                DiscardPendingChanges();
                throw e;
            }
        }

        public void CallAPlayer(string player, string partita)
        {
            try
            {
                CONVOCAZIONI cv = new CONVOCAZIONI
                {
                    Giocatore = player,
                    Partita = partita
                };
                db.CONVOCAZIONI.InsertOnSubmit(cv);
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                DiscardPendingChanges();
                throw new Exception(CONVOCAZIONI_STRING);
            }
        }

        public void checkPlayerLogin(string tesseraFir)
        {
            var player = (from g in db.GIOCATORI
                          where g.Cod_tessera_Fir_Giocatore == tesseraFir
                          select g).First();

            if (player != null)
            {
                this.CurrentPlayer = player;
            }
        }

        public void NewCoach(string tesseraFir, string nome, string cognome, string CF)
        {
            try
            {
                ALLENATORI a = new ALLENATORI
                {
                    Cod_tessera_Fir_Allenatore = tesseraFir,
                    Nome = nome,
                    Cognome = cognome,
                    CF = CF
                };

                db.ALLENATORI.InsertOnSubmit(a);
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                DiscardPendingChanges();
                throw new Exception(ADDALLENATORE_STRING);
            }
            DiscardPendingChanges();
        }

        public void NewPlayer(string tesseraFir, string nome, string cognome, string CF, string categoria, string ruolo, string lastPayment, string lastMedicalVisit)
        {
            categoria = exstractCategory(categoria);
            try
            {
                GIOCATORI g = new GIOCATORI
                {
                    Cod_tessera_Fir_Giocatore = tesseraFir,
                    Nome = nome,
                    Cognome = cognome,
                    CF = CF,
                    Categoria = Int32.Parse(categoria),
                    Ruolo = ruolo.Equals("") ? null : ruolo,
                    Data_ultimo_pagamento_iscrizione = lastPayment.Equals("") ? (DateTime?)null : Convert.ToDateTime(lastPayment),
                    Data_ultima_visita_medica = lastMedicalVisit.Equals("") ?  (DateTime?)null : Convert.ToDateTime(lastMedicalVisit)
                };
                db.GIOCATORI.InsertOnSubmit(g);
                db.SubmitChanges();

            }
            catch (Exception e)
            {
                DiscardPendingChanges();
                db.SubmitChanges();
                throw e;
            }
            DiscardPendingChanges();
        }

        public void ShowCategoryCoach(DataGridView grid)
        {
            grid.DataSource = from a in db.ALLENATORI
                              from ca in db.ASSEGNAMENTI_CATEGORIE_ALLENATORI
                              where a.Cod_tessera_Fir_Allenatore == ca.Allenatore
                              select new { a.Nome, a.Cognome, Categoria = nameCategory(ca.Categoria) };


        }

        public void ShowChampionships(DataGridView grid)
        {
            grid.DataSource = from c in db.CAMPIONATI
                              select new { c.Anno_Inizio, Categoria = nameCategory(c.Categoria), c.Piazzamento_RFC };
        }

        public void ShowPlayerMatch(DataGridView grid)
        {
            DateTime d = DateTime.Today;
            grid.DataSource = from c in db.CONVOCAZIONI
                              from p in db.PARTITE
                              where p.CodicePartita == c.Partita &&
                                    c.Giocatore == CurrentPlayer.Cod_tessera_Fir_Giocatore &&
                                    p.Data >= d
                              select new { p.Data, p.Orario, p.Squadra_Avversaria, Categoria = nameCategory(p.Categoria) };
                                        
        }

        public void ShowSpecialEvent(DataGridView grid)
        {
            DateTime d = DateTime.Today;
            grid.DataSource = from ev in db.EVENTI_ECCEZIONALI
                              where ev.Data >= d
                              select new { ev.Nome_evento, ev.Data, ev.Orario };
            
        }

        public void ShowWhoHaveToPay(DataGridView grid)
        {
            DateTime d = DateTime.Today;
            grid.DataSource = from g in db.GIOCATORI
                              where (d - g.Data_ultimo_pagamento_iscrizione).Value.Days > 365 || 
                                    g.Data_ultimo_pagamento_iscrizione.Value == null
                              select new { g.Nome, g.Cognome };
        }

        public void ShowWhoNeedsMedicalPermission(DataGridView grid)
        {
            DateTime d = DateTime.Today;
            grid.DataSource = from g in db.GIOCATORI
                              where (d - g.Data_ultima_visita_medica).Value.Days > 365 ||
                                    g.Data_ultima_visita_medica.Value == null
                              select new { g.Nome, g.Cognome };
            
        }

        public void ShowTraining(DataGridView grid)
        {
            DateTime d = DateTime.Today;
            grid.DataSource = from all in db.ALLENAMENTI
                              where all.Data >= d && all.Categoria == currentCategory()
                              select new { all.Data, all.Orario, Categoria = nameCategory(all.Categoria), all.Descrizione };
        }

        public void reset()
        {
            CurrentCoach = null;
            CurrentPlayer = null;
        }

        private int currentCategory()
        {
            if (CurrentPlayer != null) return CurrentPlayer.Categoria;
            else return (from ac in db.ASSEGNAMENTI_CATEGORIE_ALLENATORI
                         where ac.Allenatore == CurrentCoach.Cod_tessera_Fir_Allenatore
                         select ac).First().Categoria;
        }

        public void ShowCoachMatch(DataGridView grid)
        {
            DateTime d = DateTime.Today;
            grid.DataSource = from p in db.PARTITE
                              where p.Categoria == currentCategory() &&
                                    p.Data >= d
                              select new { p.Data, p.Orario, p.Squadra_Avversaria, Categoria = nameCategory(p.Categoria), p.CodicePartita};
        }

        public void addTrainig(string spazio, string data, string orario, string durata, string descrizione)
        {
            try
            {
                PRENOTAZIONI pr = new PRENOTAZIONI
                {
                    Data = Convert.ToDateTime(data),
                    Spazio = spazio,
                    Orario = ParseStringToTime(orario),
                    Durata = Int32.Parse(durata),
                };

                db.PRENOTAZIONI.InsertOnSubmit(pr);
                db.SubmitChanges();

                ALLENAMENTI al = new ALLENAMENTI
                {
                    Data = Convert.ToDateTime(data),
                    Spazio = spazio,
                    Orario = ParseStringToTime(orario),
                    Descrizione = descrizione,
                    Categoria = currentCategory()

                };
                db.ALLENAMENTI.InsertOnSubmit(al);
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                DiscardPendingChanges();
                throw new Exception(ADDALLENAMENTI_STRING);
            }
        }

        public void showCategoryPlayers(DataGridView grid)
        {
            grid.DataSource = from g in db.GIOCATORI
                              where g.Categoria == currentCategory()
                              select new { g.Nome, g.Cognome, Categoria = nameCategory(g.Categoria), Tessera = g.Cod_tessera_Fir_Giocatore };
        }

        public void AddMatch(string categoria, string stagione, string orario, string data, char inCasa, string avversaria)
        {
            categoria = exstractCategory(categoria);
            string CodPartita = categoria + stagione + data;
            if (inCasa.Equals('s'))
            {
                PRENOTAZIONI pr = new PRENOTAZIONI
                {
                    Data = Convert.ToDateTime(data),
                    Orario = ParseStringToTime(orario),
                    Spazio = "Campo",
                    Durata = 2
                };

                PRENOTAZIONI pC = new PRENOTAZIONI
                {
                    Data = Convert.ToDateTime(data),
                    Orario = ParseStringToTime(orario).Add(new TimeSpan(3, 0, 0)), //dovrebbe in teoria aggiungere la prenotazione per la club house
                    Spazio = "Club House",
                    Durata = 1
                };

                PARTITE pa = new PARTITE
                {
                    Data = Convert.ToDateTime(data),
                    Orario = ParseStringToTime(orario),
                    Categoria = Int32.Parse(categoria),
                    Anno_Inizio_Campionato = Int32.Parse(stagione),
                    In_casa = 'y',
                    Squadra_Avversaria = avversaria,
                    CodicePartita = CodPartita
                };

                PRENOTAZIONI_PARTITE prenPartClub = new PRENOTAZIONI_PARTITE
                {
                    Data = Convert.ToDateTime(data),
                    Orario = pC.Orario,
                    Spazio = "Club House",
                    Partita = CodPartita
                };

                PRENOTAZIONI_PARTITE prenPartCampo = new PRENOTAZIONI_PARTITE
                {
                    Data = Convert.ToDateTime(data),
                    Orario = pr.Orario,
                    Spazio = "Campo",
                    Partita = CodPartita
                };
                try
                {
                    // prenotazione partita
                    db.PARTITE.InsertOnSubmit(pa);
                    db.SubmitChanges();
                } catch (Exception ec)
                {
                    DiscardPendingChanges();
                    throw (ec);
                }
                try
                {
                    //prenotazione campo
                    db.PRENOTAZIONI.InsertOnSubmit(pr);
                    db.SubmitChanges();
                } catch (Exception e)
                {
                    DiscardPendingChanges();
                    db.PARTITE.DeleteOnSubmit(pa);  db.SubmitChanges(); throw e;
                }
                try
                {
                    //prenotazione club house
                    db.PRENOTAZIONI.InsertOnSubmit(pC);
                    db.SubmitChanges();
                }
                catch (Exception e)
                {
                    DiscardPendingChanges();
                    db.PARTITE.DeleteOnSubmit(pa); db.PRENOTAZIONI.DeleteOnSubmit(pr);  db.SubmitChanges(); throw e;
                }

                try
                {
                    db.PRENOTAZIONI_PARTITE.InsertOnSubmit(prenPartCampo);
                    db.SubmitChanges();
                } catch (Exception e)
                {
                    DiscardPendingChanges();
                    db.PARTITE.DeleteOnSubmit(pa); db.PRENOTAZIONI.DeleteOnSubmit(pr); db.PRENOTAZIONI.DeleteOnSubmit(pC);
                    db.SubmitChanges(); throw e;
                }

                try
                {
                    db.PRENOTAZIONI_PARTITE.InsertOnSubmit(prenPartClub);
                    db.SubmitChanges();
                }
                catch (Exception e)
                {
                    DiscardPendingChanges();
                    db.PARTITE.DeleteOnSubmit(pa); db.PRENOTAZIONI.DeleteOnSubmit(pr); db.PRENOTAZIONI.DeleteOnSubmit(pC);
                    db.PRENOTAZIONI_PARTITE.DeleteOnSubmit(prenPartCampo);
                    db.SubmitChanges(); throw e;
                }
            } else
            {
                PARTITE pa = new PARTITE
                {
                    Data = Convert.ToDateTime(data),
                    Orario = ParseStringToTime(orario),
                    Categoria = Int32.Parse(categoria),
                    Anno_Inizio_Campionato = Int32.Parse(stagione),
                    In_casa = 'n',
                    Squadra_Avversaria = avversaria,
                    CodicePartita = CodPartita
                };
                try
                {
                    // prenotazione partita
                    db.PARTITE.InsertOnSubmit(pa);
                    db.SubmitChanges();
                }
                catch (Exception ec)
                {
                    DiscardPendingChanges();
                    throw (ec);
                }
            }

        }

       

        public void PlayerStatistics(string tesseraFir, DataGridView grid)
        {
            var player = (from p in db.GIOCATORI
                          where p.Cod_tessera_Fir_Giocatore == tesseraFir
                          select p).First();

            //check per verificare che il giocatore sia sotto la responsabilità di quell'allenatore
            if (player.Categoria != currentCategory())
            {
                throw new Exception(NON_E_IL_TUO_GIOCATORE);
            }
            grid.DataSource = from ep in db.EPISODI
                              from p in db.PARTITE
                              where p.CodicePartita == ep.Partita && ep.Giocatore == tesseraFir
                              select new { ep.Tipo, p.Data, p.Squadra_Avversaria, Minuto = ep.Secondo / 60 };
        }

        public void CurrentPlayerStatistics(DataGridView grid)
        {
            grid.DataSource = from ep in db.EPISODI
                              from p in db.PARTITE
                              where p.CodicePartita == ep.Partita && ep.Giocatore == CurrentPlayer.Cod_tessera_Fir_Giocatore
                              select new { ep.Tipo, p.Data, p.Squadra_Avversaria, Minuto = ep.Secondo / 60 };
        }

        public void AddMatchEvents(string player, string type, string second, string season, string date, string data_fine)
        {
            string CodPartita = currentCategory() + season + date;

            try
            {
                EPISODI ep = new EPISODI
                {
                    Giocatore = player,
                    Partita = CodPartita,
                    Secondo = Int32.Parse(second),
                    Tipo = type,
                    Data_fine = data_fine.Equals("") ? (DateTime?)null : DateTime.Parse(data_fine)
                };

            
                db.EPISODI.InsertOnSubmit(ep);
                db.SubmitChanges();
            }
            catch (Exception ec)
            {
                DiscardPendingChanges();
                throw (ec);
            }
        }

        public void AddSpecialEvents(string data, string responsabile, string nomeEvento, string time, string duration, string spazio)
        {
            try
            {
                PRENOTAZIONI pr = new PRENOTAZIONI
                {
                    Data = Convert.ToDateTime(data),
                    Spazio = spazio,
                    Orario = ParseStringToTime(time),
                    Durata = Int32.Parse(duration),
                };

                db.PRENOTAZIONI.InsertOnSubmit(pr);
                db.SubmitChanges();

                EVENTI_ECCEZIONALI ec = new EVENTI_ECCEZIONALI
                {
                    Data = Convert.ToDateTime(data),
                    Spazio = spazio,
                    Orario = ParseStringToTime(time),
                    Nome_evento = nomeEvento,
                    Responsabile = responsabile
                };
                db.EVENTI_ECCEZIONALI.InsertOnSubmit(ec);
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                DiscardPendingChanges();
                throw new Exception(ADD_EVENT_ERROR);
            }

        }

        public void showPrenotazioni(DataGridView grid)
        {
            DateTime d = DateTime.Today;
            grid.DataSource = from p in db.PRENOTAZIONI
                              where p.Data >= d
                              select new { p.Data, p.Spazio, p.Orario, p.Durata };
                              
        }

        //utility
        private TimeSpan ParseStringToTime(string orario)
        {
            return new TimeSpan(Int32.Parse(String.Concat(orario[0], orario[1])), Int32.Parse(String.Concat(orario[3], orario[4])), 0);
        }

        private void DiscardPendingChanges()
        {
            /*
            ChangeSet changeSet = this.db.GetChangeSet();
            if (changeSet != null)
            {
                //Undo inserts
                foreach (object objToInsert in changeSet.Inserts)
                {
                    db.GetTable(objToInsert.GetType()).DeleteOnSubmit(objToInsert);
                }
                //Undo deletes
                foreach (object objToDelete in changeSet.Deletes)
                {
                    db.GetTable(objToDelete.GetType()).InsertOnSubmit(objToDelete);
                }
            }*/
            db = new DataBaseDataContext();

        }

        private string exstractCategory(string Categoria)
        {
            if (Categoria.Equals("Senior")) return "42";
            return String.Concat(Categoria[5], Categoria[6]);
        }

        private string nameCategory(int category)
        {
            return category == 42 ? "Senior" : String.Concat("Under", category);
        }
    }
}
